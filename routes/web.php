<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\WebController;

// Route::get('/', function () {
// 	return view('web.index');
// })->name('/');

Route::get('/', [WebController::class, 'index'])->name('/');

Route::get('/dashboard', function () {
    return view('dboard.dboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
require __DIR__.'/dboard.php';
