<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dboard\DboardController;
use App\Http\Controllers\Settings\MenuController;
use App\Http\Controllers\Settings\PageController;
use App\Http\Controllers\Settings\SectionController;
use App\Http\Controllers\Content\TextContentController;
use App\Http\Controllers\Content\ImageContentController;
use App\Http\Controllers\Content\FileContentController;
use App\Http\Controllers\Content\VideoContentController;

Route::group(['prefix' => 'dboard', 'as' => 'dboard.', 'middleware' => ['auth','dboard']], function () {
    Route::get('/', [DboardController::class, 'dboard'])->name('/');
    // menus
	Route::resource('menu', MenuController::class);
	Route::get('menu/{id}/delete', [MenuController::class, 'destroy'])->name('menu.delete');
    // page
 	Route::resource('page', PageController::class);
	Route::get('page/{id}/delete', [PageController::class, 'destroy'])->name('page.delete');
	// section
	Route::resource('section', SectionController::class);
	Route::get('section/{id}/create', [SectionController::class, 'create'])->name('section.create');
	Route::get('section/{id}/delete', [SectionController::class, 'destroy'])->name('section.delete');
	Route::post('section/store-heading', [SectionController::class, 'storeHeading'])->name('section.store-heading');
	// image contents 
	Route::resource('image-content', ImageContentController::class);
	Route::get('image-content/{id}/create', [ImageContentController::class, 'create'])->name('image-content.create');
	Route::get('image-content/{id}/status', [ImageContentController::class, 'status'])->name('image-content.status');
	Route::get('image-content/{id}/delete', [ImageContentController::class, 'destroy'])->name('image-content.delete');
	// text contents 
	Route::resource('text-content', TextContentController::class);
	Route::get('text-content/{id}/create', [TextContentController::class, 'create'])->name('text-content.create');
	Route::get('text-content/{id}/status', [TextContentController::class, 'status'])->name('text-content.status');
	Route::get('text-content/{id}/delete', [TextContentController::class, 'destroy'])->name('text-content.delete');
	// file contents 
	Route::resource('file-content', FileContentController::class);
	Route::get('file-content/{id}/create', [FileContentController::class, 'create'])->name('file-content.create');
	Route::get('file-content/{id}/status', [FileContentController::class, 'status'])->name('file-content.status');
	Route::get('file-content/{id}/delete', [FileContentController::class, 'destroy'])->name('file-content.delete');
	// video contents 
	Route::resource('video-content', VideoContentController::class);
	Route::get('video-content/{id}/create', [VideoContentController::class, 'create'])->name('video-content.create');
	Route::get('video-content/{id}/status', [VideoContentController::class, 'status'])->name('video-content.status');
	Route::get('video-content/{id}/delete', [VideoContentController::class, 'destroy'])->name('video-content.delete');
});