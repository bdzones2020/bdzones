<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $passSystem = 789456123;
        $userSystem = User::create([
            'name' => 'System Admin',
            'email' => 'mosharof.bdzones@gmail.com',
            'type' => 'system',
            'weight' => 99.99,
            'status' => 1,
            'password' => bcrypt($passSystem),
            //'remember_token' => str_random(10),
        ]);
    }
}
