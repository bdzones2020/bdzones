<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_contents', function (Blueprint $table) {
            $table->id();
            $table->integer('section_id')->index()->unsigned()->nullable()->default(0);
            $table->integer('text_content_id')->index()->unsigned()->nullable()->default(0);
            $table->string('type')->nullable();
            $table->string('title')->nullable();
            $table->string('file')->nullable();
            $table->text('description')->nullable();
            $table->integer('total_view')->nullable()->default(0);
            $table->integer('serial_no')->nullable()->default(0);
            $table->boolean('status')->nullable()->default(false);
            $table->integer('created_by')->unsigned()->nullable()->default(0);
            $table->integer('updated_by')->unsigned()->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_contents');
    }
}
