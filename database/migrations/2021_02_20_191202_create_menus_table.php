<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id')->index()->unsigned()->nullable()->default(0);
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('positions')->nullable();
            $table->string('type')->nullable();
            $table->integer('level')->nullable()->default(0);
            $table->integer('serial_no')->nullable()->default(0);
            $table->boolean('status')->nullable()->default(false);
            $table->integer('created_by')->unsigned()->nullable()->default(0);
            $table->integer('updated_by')->unsigned()->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
