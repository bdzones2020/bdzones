<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->id();
            $table->integer('page_id')->index()->unsigned()->nullable()->default(0);
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('heading')->nullable();
            $table->string('property')->nullable();
            $table->string('image')->nullable();
            $table->string('display_type')->nullable();
            $table->integer('serial_no')->nullable()->default(0);
            $table->boolean('status')->nullable()->default(false);
            $table->integer('created_by')->unsigned()->nullable()->default(0);
            $table->integer('updated_by')->unsigned()->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sections');
    }
}
