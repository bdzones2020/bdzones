        <!-- ======= Top Bar ======= -->
        <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
            <div class="container d-flex">
                <div class="contact-info mr-auto"><i class="icofont-envelope"></i> <a href="mailto:{{ config('app.email') }}">{{ config('app.email') }}</a> <a href="tel:{{ config('app.contact_no') }}"><i class="icofont-phone"></i> {{ config('app.contact_no') }}</a></div>
                <div class="social-links">
                    <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
                    <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
                    <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
                    <a href="#" class="skype"><i class="icofont-skype"></i></a>
                    <a href="#" class="linkedin"><i class="icofont-linkedin"></i></a>
                </div>
            </div>
        </div>
        
        <!-- ======= Header ======= -->
        <header id="header" class="fixed-top">
            <div class="container d-flex align-items-center">
                <!-- <h1 class="logo mr-auto"><a href="{{ route('/') }}">{{ config('app.name') }}</a></h1> -->
                <!-- Uncomment below if you prefer to use an image logo -->
                 <a href="{{ route('/') }}" class="logo mr-auto"><img src="{{ asset('img/logo.png') }}" alt="" class="img-fluid"></a>

                <nav class="nav-menu d-none d-lg-block mr-auto">
                    <ul>
                        <li class="active"><a href="#hero">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#services">Services</a></li>
                        <li><a href="#portfolio">Portfolio</a></li>
                        <li><a href="#team">Team</a></li>
                        <!-- <li class="drop-down">
                            <a href="">Drop Down</a>
                            <ul>
                                <li><a href="#">Drop Down 1</a></li>
                                <li class="drop-down">
                                    <a href="#">Deep Drop Down</a>
                                    <ul>
                                        <li><a href="#">Deep Drop Down 1</a></li>
                                        <li><a href="#">Deep Drop Down 2</a></li>
                                        <li><a href="#">Deep Drop Down 3</a></li>
                                        <li><a href="#">Deep Drop Down 4</a></li>
                                        <li><a href="#">Deep Drop Down 5</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Drop Down 2</a></li>
                                <li><a href="#">Drop Down 3</a></li>
                                <li><a href="#">Drop Down 4</a></li>
                            </ul>
                        </li> -->
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </nav>
                <!-- .nav-menu -->

                @auth
                <div class="dropdown" id="user-nav">
                    <span type="button" class="cur-pointer d-inline ml-3 dropdown-toggle" data-toggle="dropdown">
                        {{ Auth::user()->name }}
                    </span>
                    <div class="dropdown-menu">
                        <a class="dropdown-item text-danger" href="{{ route('dboard./') }}">Dashboard</a>
                        <a class="dropdown-item" href="#">Account</a>
                        <a class="dropdown-item" href="#">Logout</a>
                    </div>
                </div>
                @endauth

                <!-- <a href="#about" class="get-started-btn scrollto">Get Started</a> -->
            </div>
        </header>
        <!-- End Header -->