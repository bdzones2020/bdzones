<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="descriptison" content="{{ config('app.name') }} , @yield('meta_description')" />
        <meta name="keywords" content="{{ config('app.name') }} , @yield('meta_keywords')" />

        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <title>{{ config('app.name') }} @yield('title',config('app.name'))</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" />

        <!-- Styles -->
        <!-- <link rel="stylesheet" href="{{ asset('css/app.css') }}" /> -->

        <!-- Favicons -->
        <link rel="icon" href="{{ asset('img/icon.png') }}" />
        <link rel="apple-touch-icon" href="{{ asset('img/icon.png') }}" />

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet" />

        <!-- Vendor CSS Files -->
        <link rel="stylesheet" href="{{ asset('web/assets/vendor/bootstrap/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('web/assets/vendor/icofont/icofont.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('web/assets/vendor/boxicons/css/boxicons.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('web/assets/vendor/animate.css/animate.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('web/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('web/assets/vendor/venobox/venobox.css') }}" />

        <!-- Template Main CSS File -->
        <link rel="stylesheet" href="{{ asset('web/assets/css/style.css') }}" />
        <link rel="stylesheet" href="{{ asset('web/assets/css/custom.css') }}" />

        <!-- Scripts -->
        <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
    </head>
    <body>

        @include('web.inc.header')

        <main id="main">

            @yield('web_content')

        </main>
        <!-- End #main -->

        @include('web.inc.footer')

        <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

        <!-- Vendor JS Files -->
        <script src="{{ asset('web/assets/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('web/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}" defer></script>
        <script src="{{ asset('web/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('web/assets/vendor/php-email-form/validate.js') }}"></script>
        <script src="{{ asset('web/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('web/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
        <script src="{{ asset('web/assets/vendor/venobox/venobox.min.js') }}"></script>

        <!-- Template Main JS File -->
        <script src="{{ asset('web/assets/js/main.js') }}"></script>
    </body>
</html>
