<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicons -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/icon.png') }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}" defer></script>
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-migrate-3.3.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}" crossorigin="anonymous"></script>

    @stack('dataTable')

    <!-- font awesome -->
    <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.css') }}" rel="stylesheet" type="text/css">

    <!-- Fonts -->
    <!-- <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css2?family=Heebo&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/nav-sidebar.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
    
    @stack('styles')

</head>
<body>
    <div id="app">
        
        @include('dboard.inc.header')

        <main>

            @auth

            <div id="wrapper">

                <!-- Sidebar -->
                @include('dboard.inc.sidebar')
                <!-- /#sidebar-wrapper -->

                <!-- Page Content -->
                <div id="page-content-wrapper">

                    <div class="breadcrumb-box">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Dashboard</a></li>
                            @yield('breadcrumbs')
                        </ul>
                    </div>

                    <div class="page-content">

                        @include('dboard.inc.message')
                        
                        @yield('dboard_content')

                    </div>

                    <hr>

                    @include('dboard.inc.footer')

                </div>
                <!-- /#page-content-wrapper -->

            </div>

            @endauth

        </main>
    </div>

    <!-- Menu Toggle Script -->
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip({
                trigger : 'hover'
            });
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
                $('#navbarSupportedContent').collapse('hide');
            });
            $('#navbar-toggler').click(function(){
                //e.preventDefault();
                $("#wrapper").removeClass('toggled');
                $('#navbarSupportedContent').collapse('toggle');
            });
            $('#page-content-wrapper').click(function(){
                $("#wrapper").removeClass("toggled");
                $('#navbarSupportedContent').collapse('hide');
            });
            $('.d-alert').fadeOut(3000);
        });
    </script>

    @stack('scripts')

</body>
</html>
