@extends('dboard.index')

@section('title','Dashboard')

@section('dboard_content')

<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <h4 class="page-header">
                <i class="fa fa-dashboard"></i> Dashboard
            </h4>                    
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection