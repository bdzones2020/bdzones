@extends('dboard.index')
@section('title','Pages')

@push('dataTable')
    @include('dboard.inc.dataTable')
@endpush

@php $urlArray = explode('/', getFullUrl()) @endphp

@section('breadcrumbs')

    <li class="breadcrumb-item active"><a href="#">Pages</a></li>

@endsection

@push('dataTableAssets')    
    @include('dboard.table_helper.dataTable')
@endpush

@section('dboard_content')

<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <h4 class="page-header">
                <i class="fa fa-file-o"></i>
                Pages
            </h4>                    
        </div>
    </div>

    <hr class="heading-devider">

    @include('dboard.table_helper.table_switcher')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">

                        <table class="table table-sm table-striped table-hover table-bordered table-condensed" id="displayTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl.</th>
                                    <th nowrap="">Title</th>
                                    <th nowrap="">Type</th>
                                    <th nowrap="">Image</th>
                                    <th nowrap="">Contents / Details</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!empty($pages))
                                @foreach($pages as $k => $page)
                                <tr>
                                    <td>{{ $k+1 }}</td>
                                    <td>{{ $page->title }}</td>
                                    <td>{{ $page->type }}</td>
                                    <td>
                                        @if($page->image)
                                        <img src="{{ asset('img_content/'.$page->image) }}" alt="" class="img-fluid img-cell">
                                        @else
                                        N/A
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-xs">
                                            
                                            <a type="button" href="{{ route('dboard.page.show',$page->id) }}" class="btn btn-sm btn-info" title="Contents / Details of {{ $page->title }}" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye"></i> Contents / Details</a>

                                            @if(Auth::user()->type == 'system')

                                            <a type="button" href="{{ route('dboard.page.delete',$page->id) }}" class="btn btn-sm btn-danger" title="Delete {{ $page->title }}" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash"></i></a>

                                            @endif

                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection

@push('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        //
    });
</script>

@endpush