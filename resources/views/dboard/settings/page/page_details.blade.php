@extends('dboard.index')
@section('title', $page->title. ' - Page Details')

@php $urlArray = explode('/', getFullUrl()) @endphp

@push('styles')
    <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('plugins/flatpicker-4.5.7/css/flatpicker-4.5.7.min.css') }}">
    <script src="{{ asset('plugins/flatpicker-4.5.7/js/flatpicker-4.5.7.min.js') }}"></script>
@endpush

@section('breadcrumbs')

    <li class="breadcrumb-item"><a href="{{ route('dboard.page.index') }}">Pages</a></li>
    <li class="breadcrumb-item active"><a href="#">{{ $page->title. ' - Page Details' }}</a></li>

@endsection

@section('dboard_content')

<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <h4 class="page-header">
                <i class="fa fa-file-o"></i> Page Details - <span class="text-success">{{ $page->title }}</span>

                @if(Auth::user()->weight >= 59.99)
                    
                    <span class=""><a href="{{ route('dboard.page.edit',$page->id) }}" class="btn btn-sm btn-link"><i class="fa fa-edit"></i> Edit Page</a></span>


                    @if($page->type == 'dynamic')
                    
                        <span class="pull-right"><button id="addSectionBtn" data-target="#addSectionModal" data-toggle="modal" data-backdrop="static" class="btn btn-sm btn-primary">Create Section</button></span>

                    @endif

                @endif
            </h4>                    
        </div>
    </div>

    <hr class="heading-devider">

    <div class="row">
        <div class="col-12">
            <small class="text-danger">Please, DO NOT EDIT Section Name, This is for system use only.</small>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- <div class="card-body" style="background-image: url({{ asset('img_content/'.$page->image) }})"> -->
                <div class="card-body">

                    @if(count($page->sections) > 0)

                        @foreach($page->sections as $s => $section)

                        <div class="container-fluid pl-0 pr-0">
                            <div class="row bg-light">
                                <div class="col-12 click-cursor" data-toggle="collapse" data-target="#details{{ str_replace(' ','',$section->id) }}">
                                    <h5 class="mt-2">
                                        {{ $section->name }} <small>({{ $section->type == 'groupContent' ? $section->type.'-'.getGroupContentType($section->property) : $section->type }})</small>

                                        <!-- <span class=""><button id="{{ $section->id }}" type="button" class="btn btn-sm btn-link displayTypeBtn" data-name="{{ $section->type }}"><i class="fa fa-circle"></i> Display Type: <span class="text-dark">{{ $section->display_type ? $section->display_type : 'default' }}</span></button></span> -->

                                        @if(Auth::user()->weight >= 59.99)

                                        <span class=""><a href="{{ route('dboard.section.edit',$section->id) }}" class="btn btn-sm btn-link"><i class="fa fa-edit"></i> Edit</a></span>
                                        <span class=""><a href="{{ route('dboard.section.delete',$section->id) }}" class="btn btn-sm btn-link text-danger"><i class="fa fa-trash"></i> Delete</a></span>

                                        @endif

                                        <span class="pull-right">

                                            @if($section->type == 'imageSlider')

                                            <a href="{{ route('dboard.image-content.create',$section->id) }}" class="btn btn-sm btn-primary">Add Slide Image</a>

                                            @elseif($section->type == 'article')

                                            <a href="{{ route('dboard.text-content.create',$section->id) }}" class="btn btn-sm btn-primary">Add Article</a>

                                            @elseif($section->type == 'msWord')

                                            <a href="{{ route('dboard.text-content.create',$section->id) }}" class="btn btn-sm btn-primary">Add MS Text</a>

                                            @elseif($section->type == 'groupContent')

                                            <a href="{{ route('dboard.'.strtolower(getGroupContentType($section->property)).'-content.create',$section->id) }}" class="btn btn-sm btn-primary">Add {{ getGroupContentType($section->property) }}</a>

                                            @endif

                                        </span>
                                    </h5>
                                </div>
                            </div>
                            <div class="row collapse show" id="details{{ str_replace(' ','',$section->id) }}">

                                <div class="container-fluid">
                                    
                                    @if(in_array('sectionHeading', explode(',',$section->property)))
                                    <form action="{{  route('dboard.section.store-heading') }}" method="post">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="sectionId" value="{{ $section->id }}">
                                            <div class="col-12">
                                                <small class="text-danger">(Section Heading Max: 190 Character)</small>
                                                <div class="input-group input-group-sm mb-2">
                                                    <div class="input-group-prepend">
                                                       <span class="input-group-text">Section Heading</span>
                                                    </div>
                                                    <input type="text" class="form-control" name="heading" value="{{ $section->heading ? $section->heading : '' }}" placeholder="Add Section Heading here ...">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-success" type="submit">Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    @endif

                                    <div class="row">
                                        
                                        @if($section->type == 'imageSlider')

                                            @include('dboard.content.image_content.contents')

                                            <hr class="heading-devider mt-2">

                                        @elseif($section->type == 'article')

                                            @include('dboard.content.text_content.contents')

                                            <hr class="heading-devider mt-2">

                                        @elseif($section->type == 'msWord')

                                            @include('dboard.content.text_content.contents')

                                            <hr class="heading-devider mt-2">

                                        @elseif($section->type == 'groupContent')

                                            @if(strtolower(getGroupContentType($section->property)) == 'image')

                                                @include('dboard.content.image_content.contents')

                                                <hr class="heading-devider mt-2">

                                            @endif

                                            @if(strtolower(getGroupContentType($section->property)) == 'text')

                                                @include('dboard.content.text_content.contents')

                                                <hr class="heading-devider mt-2">

                                            @endif

                                            @if(strtolower(getGroupContentType($section->property)) == 'video')

                                                @include('dboard.content.video_content.contents')

                                                <hr class="heading-devider mt-2">

                                            @endif

                                            @if(strtolower(getGroupContentType($section->property)) == 'file')

                                                @include('dboard.content.file_content.contents')

                                                <hr class="heading-devider mt-2">

                                            @endif

                                        @endif
                                        
                                    </div>
                                </div>

                            </div>
                        </div>

                        @endforeach

                    @endif
                    
                </div>
            </div>
        </div>
    </div>
    
</div>

<div class="modal fade" id="sectionEntryModal" role="dialog">
    <div class="modal-dialog modal-lg" style="max-width: 920px;">
        <!-- Modal content-->
        <div class="modal-content">
            <form class="form-horizontal" role="form" method="POST" action="{{  route('dboard.section.store') }}" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Create Section with Content Type & Properties
                    </h4>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                </div>
                <div class="modal-body">


                    <div class="row">
                        <div class="col-12 text-center mb-2 fw500">
                            <span class="text-danger">Section Type can not be changed once it is created</span>
                        </div>
                    </div>

                    {{ csrf_field() }}

                    <input type="hidden" name="page_id" value="{{ $page->id }}">

                    @include('dboard.settings.section.section_input_helpers')

                    
                </div>
                <div class="modal-footer">
                    <button id="submitBtn" type="submit" class="btn btn-md btn-success pull-right" disabled="">Save</button>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //
    });
</script>

@endpush