@extends('dboard.index')
@section('title', !empty($editRow) ? 'Edit ' . $editRow->title : 'Create Page')

@php $urlArray = explode('/', getFullUrl()) @endphp

@push('styles')
    <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('plugins/flatpicker-4.5.7/css/flatpicker-4.5.7.min.css') }}">
    <script src="{{ asset('plugins/flatpicker-4.5.7/js/flatpicker-4.5.7.min.js') }}"></script>
@endpush

@section('breadcrumbs')

    <li class="breadcrumb-item"><a href="{{ route('admin.page.index') }}">Pages</a></li>
    @if(in_array('edit',$urlArray))
    <li class="breadcrumb-item"><a href="{{ route('admin.page.show',$editRow->id) }}">Pages Details</a></li>
    @endif
    <li class="breadcrumb-item active"><a href="#">{{ !empty($editRow) ? 'Edit ' . $editRow->title : 'Create Page' }}</a></li>

@endsection

@section('dboard_content')

<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <h4 class="page-header">
                <i class="fa fa-file-o"></i> {{ !empty($editRow) ? 'Edit ' . $editRow->title : 'Create Page' }}
                <span class="pull-right"><a href="{{ route('admin.user.index') }}" class="btn btn-sm btn-info">See All</a></span>
            </h4>                    
        </div>
    </div>

    <hr class="heading-devider">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <form class="form-horizontal" role="form" method="POST" action="{{ !empty($editRow) ? route('admin.page.update',$editRow->id) : route('admin.page.store') }}" enctype="multipart/form-data">

                        {{ csrf_field() }}
                        @if(!empty($editRow))
                        {{ method_field('PATCH') }}
                        @endif

                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-4">
                                @include('dboard.input_helpers.title')
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label for="type" class="col-sm-12 control-label">Type</label>
                                    <div class="col-sm-12">
                                        <label class="input-container text-success">Static
                                            <input class="view_type" type="radio" name="type" value="static" {{ !empty($editRow->type) && $editRow->type == 'static' ? 'checked' : '' }}>
                                            <span class="box-checkmark"></span>
                                        </label>
                                        <label class="input-container text-success">Dynamic
                                            <input class="view_type" type="radio" name="type" value="dynamic" {{ !empty($editRow->type) && $editRow->type == 'dynamic' ? 'checked' : '' }}>
                                            <span class="box-checkmark"></span>
                                        </label>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                @include('dboard.input_helpers.serial_no')
                            </div>
                        </div>
                        <!-- ./row -->

                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group{{ $errors->has('subtitle') ? ' has-error' : '' }}">
                                    <label for="subtitle" class="col-sm-12 control-label">Subtitle</label>
                                    <div class="col-sm-10">
                                        <input id="subtitle" type="text" class="form-control" name="subtitle" value="{{ !empty($editRow->subtitle) ? $editRow->subtitle : old('subtitle') }}" placeholder="Subtitle">
                                        @if ($errors->has('subtitle'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('subtitle') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                @include('dboard.input_helpers.status')
                            </div>
                        </div>
                        <!-- ./row -->

                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-4">
                                @include('dboard.input_helpers.image')
                            </div>
                            
                            <div class="col-sm-4">
                                @include('dboard.input_helpers.image_shape')
                            </div>

                            @if(!empty($editRow->image))
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="old_image" class="col-sm-4 control-label">Old image</label>
                                    <div class="col-sm-8">
                                        <img src="{{ asset('img_content/'.$editRow->image) }}" alt="" class="img-responsive col-sm-12 mt-3">
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <!-- ./row -->

                        @include('dboard.input_helpers.meta_information')

                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-md btn-success">{{ !empty($editRow->id) ? 'Update' : 'Save' }}</button>
                            </div>
                        </div>
                        <!-- ./row -->
                    </form>

                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection

@push('scripts')
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var editRow = {!! json_encode($editRow)!!};

        $('#type').select2({
            placeholder: "Select Type",
            allowClear: true
        });

        if(!editRow){
            $('#type').val('').trigger('change');
        }
    });
</script>

@endpush