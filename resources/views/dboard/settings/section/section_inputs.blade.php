@extends('dboard.index')
@section('title', 'Edit Section - '.$editRow->name )

@php $urlArray = explode('/', getFullUrl()) @endphp

@push('styles')
    <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('plugins/flatpicker-4.5.7/css/flatpicker-4.5.7.min.css') }}">
    <script src="{{ asset('plugins/flatpicker-4.5.7/js/flatpicker-4.5.7.min.js') }}"></script>
@endpush

@section('breadcrumbs')

    <li class="breadcrumb-item"><a href="{{ route('dboard.page.index') }}">Pages</a></li>
    @if(in_array('edit',$urlArray))
    <li class="breadcrumb-item"><a href="{{ route('dboard.page.show',$editRow->page_id) }}">Pages Details</a></li>
    @endif
    <li class="breadcrumb-item active"><a href="#">{{ 'Edit Section - '.$editRow->name  }}</a></li>

@endsection

@section('dboard_content')

<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <h4 class="page-header">
                <i class="fa fa-file"></i> {{ 'Edit Section - '.$editRow->name  }}
                <span class="pull-right"><a href="{{ route('dboard.page.show',$editRow->page_id) }}" class="btn btn-sm btn-info">Page Details</a></span>
            </h4>                    
        </div>
    </div>

    <hr class="heading-devider">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('dboard.section.update',$editRow->id) }}" enctype="multipart/form-data">

                        {{ csrf_field() }}
                        @if(!empty($editRow))
                        {{ method_field('PATCH') }}
                        @endif

                        <input type="hidden" name="page_id" value="{{ $page->id }}">

                        <div class="row">
                            @include('dboard.settings.section.section_input_helpers')
                        </div>

                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-md btn-success">{{ !empty($editRow->id) ? 'Update' : 'Save' }}</button>
                            </div>
                        </div>
                        <!-- ./row -->
                    </form>

                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection

@push('scripts')
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var editRow = {!! json_encode($editRow)!!};
    });
</script>

@endpush