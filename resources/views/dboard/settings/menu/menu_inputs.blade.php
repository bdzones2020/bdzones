@extends('dboard.index')
@section('title', !empty($editRow) ? 'Edit ' . $editRow->name : 'Create Menu')

@php $urlArray = explode('/', getFullUrl()) @endphp

@push('styles')
    <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet" />
@endpush

@section('breadcrumbs')

    <li class="breadcrumb-item"><a href="{{ route('dboard.menu.index') }}">Menus</a></li>
    <li class="breadcrumb-item active"><a href="#">{{ !empty($editRow) ? 'Edit ' . $editRow->name : 'Create Menu' }}</a></li>

@endsection

@section('dboard_content')

<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <h4 class="page-header">
                <i class="fa fa-list"></i> {{ !empty($editRow) ? 'Edit ' . $editRow->name : 'Create Menu' }}
                <span class="pull-right"><a href="{{ route('dboard.menu.index') }}" class="btn btn-sm btn-info">See All</a></span>
            </h4>                    
        </div>
    </div>

    <hr class="heading-devider">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <form class="form-horizontal" role="form" method="POST" action="{{ !empty($editRow) ? route('dboard.menu.update',$editRow->id) : route('dboard.menu.store') }}" enctype="multipart/form-data">

                        {{ csrf_field() }}
                        @if(!empty($editRow))
                        {{ method_field('PATCH') }}
                        @endif

                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('positions') ? ' has-error' : '' }}">
                                    <label for="positions" class="col-sm-4 control-label">Positions</label>
                                    <div class="col-sm-8">
                                        <label class="input-container text-info">Header
                                            <input class="view_type" type="checkbox" name="positions[]" value="header" {{ !empty($editRow->positions) && in_array('header', explode(',',$editRow->positions)) ? 'checked' : '' }}>
                                            <span class="box-checkmark"></span>
                                        </label>
                                        <label class="input-container text-info">Footer
                                            <input class="view_type" type="checkbox" name="positions[]" value="footer" {{ !empty($editRow->positions) && in_array('footer', explode(',',$editRow->positions)) ? 'checked' : '' }}>
                                            <span class="box-checkmark"></span>
                                        </label>
                                        @if ($errors->has('positions'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('positions') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label for="type" class="col-sm-12 control-label">Menu Type<i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
                                        <select name="type" id="type" class="form-control" required="true">
                                            <option value="page" {{ $editRow && $editRow->type == 'page' ? 'selected' : '' }}>Page</option>
                                            <option value="link" {{ $editRow && $editRow->type == 'link' ? 'selected' : '' }}>Link</option>
                                        </select>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4" id="pageTypeDiv">
                                <div class="form-group{{ $errors->has('page_type') ? ' has-error' : '' }}">
                                    <label for="page_type" class="col-sm-12 control-label">Page Type</label>
                                    <div class="col-sm-12">
                                        <label class="input-container text-info">Dynamic
                                            <input class="page_type" type="radio" name="page_type" value="dynamic" {{ !empty($editRow->page) && $editRow->page->type == 'dynamic' ? 'selected' : '' }}>
                                            <span class="box-checkmark"></span>
                                        </label>
                                        <label class="input-container text-info">Static
                                            <input class="page_type" type="radio" name="page_type" value="statis" {{ !empty($editRow->page) && $editRow->page->type == 'static' ? 'selected' : '' }}>
                                            <span class="box-checkmark"></span>
                                        </label>
                                        @if ($errors->has('page_type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('page_type') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./row -->

                        <!-- row -->
                        <div class="row">
                            @if(count($menus) > 0)

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
                                    <label for="parent_id" class="col-sm-12 control-label">Parent</label>
                                    <div class="col-sm-8">
                                        <select name="parent_id" id="parent_id" class="form-control">
                                            {{-- <option value=""></option> --}}
                                            @if(!empty($menus))
                                                @foreach($menus as $k => $menu)
                                                <option value="{{ $menu->id }}" {{ !empty($editRow) && $editRow->parent_id == $menu->id ? 'selected' : ''}}>{{ $menu->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('parent_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('parent_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            @endif

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('level') ? ' has-error' : '' }}">
                                    <label for="level" class="col-sm-12 control-label">level<i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
                                        <select name="level" id="level" class="form-control" required="true">
                                            <option value="0" {{ $editRow && $editRow->level == '0' ? 'selected' : '' }}>0</option>
                                            <option value="1" {{ $editRow && $editRow->level == '1' ? 'selected' : '' }}>1</option>
                                            <option value="2" {{ $editRow && $editRow->level == '2' ? 'selected' : '' }}>2</option>
                                            <option value="3" {{ $editRow && $editRow->level == '3' ? 'selected' : '' }}>3</option>
                                        </select>
                                        @if ($errors->has('level'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('level') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./row -->

                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-4">
                                @include('dboard.input_helpers.name')
                            </div>
                            <div class="col-sm-4">
                                @include('dboard.input_helpers.serial_no')
                            </div>
                            <div class="col-sm-4">
                                @include('dboard.input_helpers.status')
                            </div>
                        </div>
                        <!-- ./row -->

                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-md btn-success">{{ !empty($editRow->id) ? 'Update' : 'Save' }}</button>
                            </div>
                        </div>
                        <!-- ./row -->
                    </form>

                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection

@push('scripts')
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        let editRow = {!! json_encode($editRow)!!};

        $('#pageTypeDiv').hide();

        $('#parent_id').select2({
            placeholder: "Select Parent",
            allowClear: true
        });

        if(!editRow){
            $('#parent_id').val('').trigger('change');
        }else{
            if(editRow['parent_id'] == 0)
            {
                $('#parent_id').val('').trigger('change');
            }
        }

        $('#type').select2({
            placeholder: "Select Type",
            allowClear: true
        });

        if(!editRow){
            $('#type').val('').trigger('change');
        }

        $('#level').select2({
            placeholder: "Select Level",
            allowClear: true
        });

        $('#type').change(function(){
            let type = $('#type').val();
            if(type == 'page')
            {
                $('#pageTypeDiv').show();
            }else{
                $('#pageTypeDiv').hide();
            }
        });
    });
</script>

@endpush