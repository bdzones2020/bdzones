@extends('dboard.index')
@section('title','Menus')

@push('dataTable')
    @include('dboard.inc.dataTable')
@endpush

@php $urlArray = explode('/', getFullUrl()) @endphp

@section('breadcrumbs')

    <li class="breadcrumb-item active"><a href="#">Menus</a></li>

@endsection

@push('dataTableAssets')    
    @include('dboard.table_helper.dataTable')
@endpush

@section('dboard_content')

<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <h4 class="page-header">
                <i class="fa fa-list"></i> Menus
                <span class="pull-right"><a href="{{ route('dboard.menu.create') }}" class="btn btn-sm btn-info">Create</a></span>
            </h4>                    
        </div>
    </div>

    <hr class="heading-devider">

    @include('dboard.table_helper.table_switcher')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">

                        <table class="table table-sm table-light table-hover table-bordered table-condensed" id="displayTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl.</th>
                                    <th nowrap="">Name</th>
                                    <th nowrap="">Slug</th>
                                    <th nowrap="">Type</th>
                                    <th nowrap="">Parent</th>
                                    <th nowrap="">Positions</th>
                                    <th nowrap="">Level</th>
                                    <th nowrap="">Serial No</th>
                                    <th nowrap="">Status</th>
                                    <th nowrap="" class="action-cell">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!empty($menus))
                                @foreach($menus as $k => $menu)
                                <tr>
                                    <td>{{ $k+1 }}</td>
                                    <td>{{ $menu->name }}</td>
                                    <td>{{ $menu->slug }}</td>
                                    <td class="{{ $menu->type == 'page' ? 'text-success' : 'text-info' }}">{{ $menu->type }}</td>
                                    <td>{{ $menu->parent ? $menu->parent->name : 'None'}}</td>
                                    <td>{{ $menu->positions }}</td>
                                    <td>{{ $menu->level }}</td>
                                    <td>{{ $menu->serial_no }}</td>
                                    <td class="text-{{ $menu->status == 1 ? 'success' : 'danger' }}">{!! $menu->status == 1 ? 'ACTIVE' : 'INACTIVE' !!}</td>
                                    <td>
                                        <div class="btn-group btn-group-xs">
                                            
                                            <a type="button" href="{{ route('dboard.menu.edit',$menu->id) }}" class="btn btn-sm btn-success" title="Edit {{ $menu->name }}" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>

                                            <a type="button" href="{{ route('dboard.menu.delete',$menu->id) }}" class="btn btn-sm btn-danger" title="Delete {{ $menu->name }}" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash"></i></a>

                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection

@push('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        //
    });
</script>

@endpush