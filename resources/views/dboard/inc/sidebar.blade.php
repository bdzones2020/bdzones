
<div id="sidebar-wrapper" class="navBg bg-dark">
    <ul class="sidebar-nav">
        <li>
            <a class="active" href=""><i class="fa fa-dashboard"></i> Dashboard</a>
        </li>
        <li>
            <a href="{{ route('/') }}" target="_blank"><i class="fa fa-home"></i> Home</a>
        </li>
        <li>
            <a class="active" href="{{ route('dboard.menu.index') }}">
                <i class="fa fa-list"></i> Menus
            </a>
        </li>
        <li>
            <a class="active" href="{{ route('dboard.page.index') }}">
                <i class="fa fa-file-o"></i> Pages
            </a>
        </li>
    </ul>
</div>