<div class="form-group{{ $errors->has('features') ? ' has-error' : '' }}">
    <label for="features" class="col-sm-8 control-label fw500">Features</label>
    <div class="col-sm-12">
        <textarea name="features" id="features" cols="1" rows="2" class="form-control" placeholder="Write Features here ...">{{ !empty($editRow->features) ? $editRow->features : old('features') }}</textarea>
        @if ($errors->has('features'))
            <span class="help-block">
                <strong>{{ $errors->first('features') }}</strong>
            </span>
        @endif
    </div>
</div>

@push('scripts')

<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
    CKEDITOR.replace('features');
    $(document).ready(function(){
        //
    });
</script>

@endpush