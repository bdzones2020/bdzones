<div class="form-group{{ $errors->has('question') ? ' has-error' : '' }}">
    <label for="question" class="col-sm-12 control-label fw500">Question<i class="text-danger">*</i></label>
    <div class="col-sm-12">
        <input id="question" type="text" class="form-control" name="question" value="{{ !empty($editRow->question) ? $editRow->question : old('question') }}" autofocus="true" placeholder="question" required="true">
        @if ($errors->has('question'))
            <span class="help-block">
                <strong>{{ $errors->first('question') }}</strong>
            </span>
        @endif
    </div>
</div>