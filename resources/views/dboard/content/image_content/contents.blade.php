@if($section->image_contents && count($section->image_contents) > 0)

    @foreach($section->image_contents as $s => $imageContent)

        <div class="col-md-3 col-sm-4 text-secondary">
            <div class="content-box">
                <img src="{{ asset('img_content/'.$imageContent->image) }}" alt="" class="img-fluid">
                <div class="btn-group-vertical pull-right">
                    <a type="button" href="{{ route('dboard.image-content.edit',$imageContent->id) }}" class="btn btn-sm btn-primary" title="Edit" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a type="button" href="{{ route('dboard.image-content.status',$imageContent->id) }}" class="btn btn-sm btn-{{ $imageContent->status ? 'success' : 'warning' }}" title="Make {{ $imageContent->status ? 'Disable' : 'Activate' }}" data-toggle="tooltip" data-placement="top"><i class="fa fa-check-square"></i></a>
                    @if(Auth::user()->weight >= '79.99')
                    <a type="button" href="{{ route('dboard.image-content.delete',$imageContent->id) }}" class="btn btn-sm btn-danger" title="Delate" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash"></i></a>
                    @endif
                </div>
                <!-- <div class="dropdown">
                    <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown">
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a type="button" href="{{ route('dboard.image-content.edit',$imageContent->id) }}" class="dropdown-item" title="Edit" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                        <a type="button" href="{{ route('dboard.image-content.status',$imageContent->id) }}" class="dropdown-item" title="Make {{ $imageContent->status ? 'Disable' : 'Activate' }}" data-toggle="tooltip" data-placement="top"><i class="fa fa-check-square"></i></a>
                        @if(Auth::user()->weight >= '79.99')
                        <a type="button" href="{{ route('dboard.image-content.delete',$imageContent->id) }}" class="dropdown-item" title="Delate" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash"></i></a>
                        @endif
                    </div>
                </div> -->

            </div>

            @if($imageContent->title)
            <div class="fw500">
                <span class="text-primary">Title</span>: {{ $imageContent->title }}
            </div>
            @endif
            @if($imageContent->caption)
            <div class="fw500">
                <span class="text-primary">Caption</span>: {{ $imageContent->caption }}
            </div>
            @endif
            @if($imageContent->link)
            <div class="fw500">
                <span class="text-primary">Link</span>: {{ $imageContent->link }}
            </div>
            @endif
            <div class="fw500">
                <span class="text-primary">Serial No</span>: {{ $imageContent->serial_no }}
            </div>
        </div>

    @endforeach

@endif