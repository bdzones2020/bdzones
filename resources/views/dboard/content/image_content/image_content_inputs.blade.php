@extends('dboard.index')
@section('title', !empty($editRow) ? 'Edit ' . $editRow->name : 'Add Image')

@php $urlArray = explode('/', getFullUrl()) @endphp

@section('breadcrumbs')

    <li class="breadcrumb-item"><a href="{{ route('dboard.page.index') }}">Pages</a></li>
    <li class="breadcrumb-item"><a href="{{ route('dboard.page.show',$section->page_id) }}">Pages Details</a></li>
    <li class="breadcrumb-item active"><a href="#">{{ !empty($editRow) ? 'Edit ' . $editRow->name : 'Add Image' }}</a></li>

@endsection

@section('dboard_content')

<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <h4 class="page-header">
                <i class="fa fa-image"></i> {{ !empty($editRow) ? 'Edit ' . $editRow->name : 'Add Image' }} <span class="text-secondary">- Section: {{ $section->name }}</span>
                <span class="pull-right"><a href="{{ route('dboard.page.show',$section->page_id) }}" class="btn btn-sm btn-info">Page Details</a></span>
            </h4>                    
        </div>
    </div>

    <hr class="heading-devider">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <form class="form-horizontal" role="form" method="POST" action="{{ !empty($editRow) ? route('dboard.image-content.update',$editRow->id) : route('dboard.image-content.store') }}" enctype="multipart/form-data">

                        {{ csrf_field() }}
                        @if(!empty($editRow))
                        {{ method_field('PATCH') }}
                        @endif

                        <input type="hidden" name="sectionId" value="{{ $section->id }}">

                        <!-- row -->
                        <div class="row">
                            
                            @if(in_array('imageTitle', explode(',',$section->property)))
                            <div class="col-sm-4">
                                @include('dboard.input_helpers.title')
                            </div>
                            @endif

                            <div class="col-sm-4">
                                @include('dboard.input_helpers.serial_no')
                            </div>
                            <div class="col-sm-4">
                                @include('dboard.input_helpers.status')
                            </div>
                        </div>
                        <!-- ./row -->
                        <!-- row -->
                        <div class="row">
                            
                            @if(in_array('imageCaption', explode(',',$section->property)))
                            <div class="col-sm-6">
                                @include('dboard.input_helpers.caption')
                            </div>
                            @endif

                            @if(in_array('imageLink', explode(',',$section->property)))
                            <div class="col-sm-6">
                                @include('dboard.input_helpers.link')
                            </div>
                            @endif

                        </div>
                        <!-- ./row -->
                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-4">
                                @include('dboard.input_helpers.image')
                            </div>
                            
                            <div class="col-sm-4">
                                @include('dboard.input_helpers.image_shape')
                            </div>

                            @if(!empty($editRow->image))
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="old_image" class="col-sm-4 control-label">Old image</label>
                                    <div class="col-sm-8">
                                        <img src="{{ asset('img_content/'.$editRow->image) }}" alt="" class="img-responsive col-sm-12 mt-3">
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <!-- ./row -->
                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-md btn-success">{{ !empty($editRow->id) ? 'Update' : 'Save' }}</button>
                            </div>
                        </div>
                        <!-- ./row -->
                    </form>

                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection

@push('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        //
    });
</script>

@endpush