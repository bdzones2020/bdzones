@if($section->text_contents && count($section->text_contents) > 0)

    @foreach($section->text_contents as $s => $textContent)

        <div class="{{ $section->type == 'groupContent' ? 'col-md-3 col-sm-4' : 'col-6' }} text-secondary">
            <div class="content-box">
                @if($textContent->image)
                <img src="{{ asset('img_content/'.$textContent->image) }}" alt="{{ $textContent->image }}" class="img-fluid img-text-content">
                @endif
                {!! $textContent->description !!}
                <div class="btn-group-vertical pull-right">
                    <a type="button" href="{{ route('dboard.text-content.edit',$textContent->id) }}" class="btn btn-sm btn-primary" title="Edit" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a type="button" href="{{ route('dboard.text-content.status',$textContent->id) }}" class="btn btn-sm btn-{{ $textContent->status ? 'success' : 'warning' }}" title="Make {{ $textContent->status ? 'Disable' : 'Activate' }}" data-toggle="tooltip" data-placement="top"><i class="fa fa-check-square"></i></a>
                    @if(Auth::user()->weight >= '79.99')
                    <a type="button" href="{{ route('dboard.text-content.delete',$textContent->id) }}" class="btn btn-sm btn-danger" title="Delate" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash"></i></a>
                    @endif
                </div>
            </div>

            @if($textContent->title)
            <div class="fw500">
                <span class="text-primary">Title</span>: {{ $textContent->title }}
            </div>
            @endif
            <div class="fw500">
                <span class="text-primary">Serial No</span>: {{ $textContent->serial_no }}
            </div>
        </div>

    @endforeach

@elseif($section->type == 'article' && $section->article)

    <div class="col-sm-12 text-secondary">
        <div class="content-box">
            @if($section->article->image)
            <img src="{{ asset('img_content/'.$section->article->image) }}" alt="{{ $section->article->image }}" class="img-fluid">
            @endif
            {!! $section->article->description !!}
            <div class="btn-group-vertical pull-right">
                <a type="button" href="{{ route('dboard.text-content.edit',$section->article->id) }}" class="btn btn-sm btn-primary" title="Edit" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                <a type="button" href="{{ route('dboard.text-content.status',$section->article->id) }}" class="btn btn-sm btn-{{ $section->article->status ? 'success' : 'warning' }}" title="Make {{ $section->article->status ? 'Disable' : 'Activate' }}" data-toggle="tooltip" data-placement="top"><i class="fa fa-check-square"></i></a>
                @if(Auth::user()->weight >= '79.99')
                <a type="button" href="{{ route('dboard.text-content.delete',$section->article->id) }}" class="btn btn-sm btn-danger" title="Delate" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash"></i></a>
                @endif
            </div>
        </div>

        @if($section->article->title)
        <div class="fw500">
            <span class="text-primary">Title</span>: {{ $section->article->title }}
        </div>
        @endif
        <div class="fw500">
            <span class="text-primary">Serial No</span>: {{ $section->article->serial_no }}
        </div>
    </div>

@endif

