<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Settings\Section;
use App\Models\Content\ImageContent;
use App\Models\Content\FileContent;
use App\Models\Content\VideoContent;

class TextContent extends Model
{
    use HasFactory;

    protected $fillable = ['section_id','type','title','image','description','serial_no','status','created_by','updated_by'];

    public function section()
    {
    	return $this->belongsTo(Section::class)->select('id','name','slug');
    }

    public function image_contents()
    {
    	return $this->hasMany(ImageContent::class);
    }

    public function active_image_contents()
    {
    	return $this->hasMany(ImageContent::class)->where('status',true)->orderBy('serial_no','ASC');
    }

    public function file_contents()
    {
    	return $this->hasMany(FileContent::class);
    }

    public function active_file_contents()
    {
    	return $this->hasMany(FileContent::class)->where('status',true)->orderBy('serial_no','ASC');
    }

    public function video_contents()
    {
    	return $this->hasMany(VideoContent::class);
    }

    public function active_video_contents()
    {
    	return $this->hasMany(VideoContent::class)->where('status',true)->orderBy('serial_no','ASC');
    }
}
