<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Settings\Section;
use App\Models\Content\TextContent;

class FileContent extends Model
{
    use HasFactory;

    protected $fillable = ['section_id','text_content_id','type','title','file','description','total_view','serial_no','status','created_by','updated_by'];

    public function section()
    {
    	return $this->belongsTo(Section::class)->select('id','name','slug');
    }

    public function text_content()
    {
    	return $this->belongsTo(TextContent::class);
    }
}
