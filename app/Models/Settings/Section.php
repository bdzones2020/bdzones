<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Settings\Page;
use App\Models\Content\TextContent;
use App\Models\Content\ImageContent;
use App\Models\Content\FileContent;
use App\Models\Content\VideoContent;

class Section extends Model
{
    use HasFactory;

    protected $fillable = ['page_id','type','name','heading','property','image','display_type','serial_no','status','created_by','updated_by'];

    public function page()
    {
    	return $this->belongsTo(Page::class)->select('id','title','slug');
    }

    public function text_contents()
    {
    	return $this->hasMany(TextContent::class)->where('type','groupContent')->orderBy('serial_no','ASC');
    }

    public function active_text_contents()
    {
        return $this->hasMany(TextContent::class)->where('type','groupContent')->select('id','section_id','title','image','description','serial_no','status')->where('status',true)->orderBy('serial_no','ASC');
    }

    public function article()
    {
        return $this->hasOne(TextContent::class)->select('id','section_id','title','image','description','serial_no','status')->where('type','article');
    }

    public function active_article()
    {
        return $this->hasOne(TextContent::class)->select('id','section_id','title','image','description','serial_no','status')->where('type','article')->where('status',true);
    }

    public function image_contents()
    {
    	return $this->hasMany(ImageContent::class)->select('id','section_id','type','image','title','caption','link','serial_no','status')->orderBy('serial_no','ASC');
    }

    public function active_image_contents()
    {
        return $this->hasMany(ImageContent::class)->select('id','section_id','image','title','caption','link','serial_no','status')->where('status',true)->orderBy('serial_no','ASC');
    }

    public function file_contents()
    {
    	return $this->hasMany(FileContent::class)->select('id','section_id','text_content_id','type','file','title','description','total_view','serial_no','status')->orderBy('serial_no','ASC');
    }

    public function active_file_contents()
    {
        return $this->hasMany(FileContent::class)->select('id','section_id','text_content_id','type','file','title','description','total_view','serial_no','status')->where('status',true)->orderBy('serial_no','ASC');;
    }

    public function video_contents()
    {
    	return $this->hasMany(VideoContent::class)->select('id','section_id','text_content_id','type','vdo_link','embed_code','title','description','total_view','serial_no','status')->orderBy('serial_no','ASC');
    }

    public function active_video_contents()
    {
        return $this->hasMany(VideoContent::class)->select('id','section_id','text_content_id','type','vdo_link','embed_code','title','description','total_view','serial_no','status')->where('status',true)->orderBy('serial_no','ASC');
    }
}
