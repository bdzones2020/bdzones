<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Settings\Page;

class Menu extends Model
{
    use HasFactory;

    protected $fillable = ['parent_id','name','slug','positions','type','level','serial_no','status','created_by','updated_by'];

    public function parent()
    {
    	return $this->belongsTo(Self::class)->select('id','parent_id','name','slug');
    }

    public function submenus()
    {
    	return $this->hasMany(Self::class, 'parent_id', 'id')->select('id','parent_id','name','slug','positions','type','level','serial_no');
    }

    public function page()
    {
    	return $this->hasOne(Page::class);
    }
}
