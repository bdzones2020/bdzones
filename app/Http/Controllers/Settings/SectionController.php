<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\Settings\Page;
use App\Models\Settings\Section;
use App\Models\Content\TextContent;
use App\Models\Content\ImageContent;
use App\Models\Content\FileContent;
use App\Models\Content\VideoContent;

use Image;
use Storage;

// helpers
use Illuminate\Support\Str;
use App\Helpers\ImageUploadHelper;

class SectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('dboard');
    }

    public function index()
    {
        //
    }

    public function create($id)
    {
        $editRow = '';
        $page = Page::find($id);
        return view('dboard.settings.section.section_inputs',compact('page','editRow'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'type' => 'required',
            'name' => 'required',
        ]);

        $section = new Section;
        $section->page_id = $request->page_id;
        $section->name = strtoupper(str_replace(' ', '_', $request->name));
        $section->type = $request->type;

        if($section->type == 'groupContent')
        {
            $properties = $request->groupContentType.','.implode(',', $request->property);
        }elseif($section->type == 'msWord'){
            $properties = $section->type;
        }else{
            $properties = implode(',', $request->property);
        }

        $section->property = $properties;
        $section->serial_no = $request->serial_no ? $request->serial_no : 0;
        $section->status = $request->status ? true : false;
        
        if ($request->hasFile('image')) {
            $storeImage = new ImageUploadHelper;
            $width = $height = null;
            if($request->shape){
                $getShape = $storeImage->shapeMaker($request->shape, $request->width, $request->height);
                $width = $getShape['width'];
                $height = $getShape['height'];
            }
            // 1 mb = 1048576 bytes in binary which is countable for the image size here
            $image_name = $storeImage->uploadImage(null, $request->file('image'), 'img_content', $section->image, 5248576, $width, $height, $section->name, ['width' => null, 'height' => null, 'thumbStorageName' => null] );
            if($image_name == 'MaxSizeErr') {
                return back()->with('message_warning', 'Too large file (max limit:1mb)');
            }
            $section->image = $image_name;
        }
        
        $section->created_by = Auth::user()->id;
        $section->save();

        return back()->with('message_success','Section has been created successfully.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $editRow = Section::with('page')->find($id);
        $page = Page::find($editRow->page_id);
        return view('dboard.settings.section.section_inputs',compact('page','editRow'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            //'type' => 'required',
            'name' => 'required',
        ]);

        $section = Section::find($id);
        $section->name = strtoupper(str_replace(' ', '_', $request->name));
        $section->serial_no = $request->serial_no;
        
        if ($request->hasFile('image')) {
            $storeImage = new ImageUploadHelper;
            $width = $height = null;
            if($request->shape){
                $getShape = $storeImage->shapeMaker($request->shape, $request->width, $request->height);
                $width = $getShape['width'];
                $height = $getShape['height'];
            }
            // 1 mb = 1048576 bytes in binary which is countable for the image size here
            $image_name = $storeImage->uploadImage(null, $request->file('image'), 'img_content', $section->image, 5248576, $width, $height, $section->name, ['width' => null, 'height' => null, 'thumbStorageName' => null] );
            if($image_name == 'MaxSizeErr') {
                return back()->with('message_warning', 'Too large file (max limit:1mb)');
            }
            $section->image = $image_name;
        }

        $section->status = $request->status ? true : false;
        $section->updated_by = Auth::user()->id;
        $section->update();

        return redirect()->route('dboard.page.show',$section->page_id)->with('message_success','Section has been updated successfully.');
    }

    public function destroy($id)
    {
        $section = Section::with(['text_contents','image_contents','file_contents','video_contents'])->find($id);

        if($section->text_contents->count() > 0)
        {
            foreach ($section->text_contents as $t => $get_text) {
                $text = TextContent::with(['image_contents','video_contents','file_contents'])->find($get_text->id);
                // image contents
                if($text->image_contents->count() > 0)
                {
                    foreach ($text->image_contents as $ti => $get_text_image) {
                        $image = ImageContent::find($get_text_image->id);
                        // check existing image
                        if($image != null) {
                            if (Storage::disk('img_content')->has($image->image)) {
                                Storage::disk('img_content')->delete($image->image);
                            }
                        }
                        $image->delete();
                    }
                }
                // file contents
                if($text->file_contents->count() > 0)
                {
                    foreach ($text->file_contents as $ti => $get_text_file) {
                        $file = FileContent::find($get_text_file->id);
                        // check existing image
                        if($file != null) {
                            if (Storage::disk('file_content')->has($file->file)) {
                                Storage::disk('file_content')->delete($file->file);
                            }
                        }
                        $file->delete();
                    }
                }
                // video contents
                if($text->video_contents->count() > 0)
                {
                    foreach ($text->video_contents as $ti => $get_text_video) {
                        $video = VideoContent::find($get_text_video->id);
                        $video->delete();
                    }
                }
                $text->delete();
            }
        }

        if($section->image_contents->count() > 0)
        {
            foreach ($section->image_contents as $ti => $get_section_image) {
                $image = ImageContent::find($get_section_image->id);
                // check existing image
                if($image != null) {
                    if (Storage::disk('img_content')->has($image->image)) {
                        Storage::disk('img_content')->delete($image->image);
                    }
                }
                $image->delete();
            }
        }

        if($section->file_contents->count() > 0)
        {
            foreach ($section->file_contents as $ti => $get_section_file) {
                $file = FileContent::find($get_section_file->id);
                // check existing image
                if($file != null) {
                    if (Storage::disk('file_content')->has($file->file)) {
                        Storage::disk('file_content')->delete($file->file);
                    }
                }
                $file->delete();
            }
        }

        if($section->video_contents->count() > 0)
        {
            foreach ($section->video_contents as $ti => $get_section_video) {
                $video = VideoContent::find($get_section_video->id);
                $video->delete();
            }
        }

        $section->delete();

        return back()->with('message_success','Section has been deleted successfully.');
    }

    public function storeHeading(Request $request)
    {
        $section = Section::find($request->sectionId);
        $section->heading = $request->heading;
        $section->update();

        return back()->with('message_success','Section Heading has been saved successfully.');
    }
}
