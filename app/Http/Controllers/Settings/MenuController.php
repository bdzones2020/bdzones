<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\Settings\Menu;
use App\Models\Settings\Page;

// helpers
use Illuminate\Support\Str;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('dboard');
    }

    public function index()
    {
        $menus = Menu::with('parent')->get();
        return view('dboard.settings.menu.menus',compact('menus'));
    }

    public function create()
    {
        $editRow = '';
        $menus = Menu::where('type','link')->get();
        return view('dboard.settings.menu.menu_inputs',compact('menus','editRow'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'type' => 'required',
            'level' => 'required',
            'positions' => 'required',
            ]);

        $menu = new Menu;
        $menu->parent_id = $request->parent_id ? $request->parent_id : 0 ;
        $menu->positions = implode(',', $request->positions);
        $menu->name = ucfirst($request->name);
        $menu->slug = Str::slug($request->name, '-');
        $menu->type = $request->type;
        $menu->level = $request->level;
        $menu->serial_no = $request->serial_no;
        $menu->status = $request->status ? true : false;
        $menu->created_by = Auth::user()->id;
        $menu->save();

        if($menu->type == 'page')
        {
            // check if page exists
            $checkPage = Page::where('slug',Str::slug($request->name, '-'))->first();

            if(!$checkPage)
            {
                $page = new Page;
                $page->menu_id = $menu->id;
                $page->title = ucfirst($request->name);
                $page->type = $request->page_type;
                $page->slug = Str::slug($request->name, '-');
                $page->status = true;
                $page->created_by = Auth::user()->id;
                $page->save();
            }
        }

        return redirect()->route('dboard.menu.index')->with('message_success','Menu has been created successfully');
    }

    public function show($id)
    {
        //return $id;
    }

    public function edit($id)
    {
        $editRow = Menu::find($id);
        $menus = Menu::where('type','link')->get();
        return view('dboard.settings.menu.menu_inputs',compact('menus','editRow'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'type' => 'required',
            'level' => 'required',
            'positions' => 'required',
            ]);

        $menu = Menu::with('page')->find($id);
        $menu->parent_id = $request->parent_id ? $request->parent_id : 0 ;
        $menu->positions = implode(',', $request->positions);
        $menu->name = $request->name;
        $menu->slug = Str::slug($request->name, '-');
        $menu->type = $request->type;
        $menu->level = $request->level;
        $menu->serial_no = $request->serial_no;
        $menu->status = $request->status ? true : false;
        $menu->updated_by = Auth::user()->id;
        $menu->update();

        if($menu->type == 'page')
        {
            $page = Page::find($menu->page->id);
            $page->title = $menu->name;
            $page->type = $request->page_type;
            $page->slug = Str::slug($request->name, '-');
            $page->updated_by = Auth::user()->id;
            $page->update();
        }

        return redirect()->route('dboard.menu.index')->with('message_success','Menu has been updated successfully');
    }

    public function destroy($id)
    {
        return $id;
        $menu = Menu::with('page')->find($id);
        // check contents 
        if($menu->type == 'page')
        {
            $page = Page::with('sections')->find($menu->page->id);

            if(count($page->sections) > 0)
            {
                foreach ($page->sections as $k => $getSection) {

                    $section = Section::with(['text_contents','image_contents','file_contents','video_contents'])->find($getSection->id);

                    if($section->text_contents->count() > 0)
                    {
                        foreach ($section->text_contents as $t => $get_text) {
                            $text = TextContent::with(['image_contents','video_contents','file_contents'])->find($get_text->id);
                            // image contents
                            if($text->image_contents->count() > 0)
                            {
                                foreach ($text->image_contents as $ti => $get_text_image) {
                                    $image = ImageContent::find($get_text_image->id);
                                    // check existing image
                                    if($image != null) {
                                        if (Storage::disk('img_content')->has($image->image)) {
                                            Storage::disk('img_content')->delete($image->image);
                                        }
                                    }
                                    $image->delete();
                                }
                            }
                            // file contents
                            if($text->file_contents->count() > 0)
                            {
                                foreach ($text->file_contents as $ti => $get_text_file) {
                                    $file = FileContent::find($get_text_file->id);
                                    // check existing image
                                    if($file != null) {
                                        if (Storage::disk('file_content')->has($file->file)) {
                                            Storage::disk('file_content')->delete($file->file);
                                        }
                                    }
                                    $file->delete();
                                }
                            }
                            // video contents
                            if($text->video_contents->count() > 0)
                            {
                                foreach ($text->video_contents as $ti => $get_text_video) {
                                    $video = VideoContent::find($get_text_video->id);
                                    $video->delete();
                                }
                            }
                            $text->delete();
                        }
                    }

                    if($section->image_contents->count() > 0)
                    {
                        foreach ($section->image_contents as $ti => $get_section_image) {
                            $image = ImageContent::find($get_section_image->id);
                            // check existing image
                            if($image != null) {
                                if (Storage::disk('img_content')->has($image->image)) {
                                    Storage::disk('img_content')->delete($image->image);
                                }
                            }
                            $image->delete();
                        }
                    }

                    if($section->file_contents->count() > 0)
                    {
                        foreach ($section->file_contents as $ti => $get_section_file) {
                            $file = FileContent::find($get_section_file->id);
                            // check existing image
                            if($file != null) {
                                if (Storage::disk('file_content')->has($file->file)) {
                                    Storage::disk('file_content')->delete($file->file);
                                }
                            }
                            $file->delete();
                        }
                    }

                    if($section->video_contents->count() > 0)
                    {
                        foreach ($section->video_contents as $ti => $get_section_video) {
                            $video = VideoContent::find($get_section_video->id);
                            $video->delete();
                        }
                    }

                    $section->delete();
                }
            }

            if($page->image)
            {
                if (Storage::disk('img_content')->has($page->image)) {
                    Storage::disk('img_content')->delete($page->image);
                }
            }

            $page->delete();
        }

        $menu->delete();
        
        return back()->with('message_success','Menu has been deleted successfully');
    }
}
