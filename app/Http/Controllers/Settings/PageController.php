<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\Settings\Menu;
use App\Models\Settings\Page;

use Image;
use Storage;

// helpers
use Illuminate\Support\Str;
use App\Helpers\ImageUploadHelper;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('dboard');
    }

    public function index()
    {
        $pages = Page::with(['sections'])->get();
        return view('dboard.settings.page.pages',compact('pages'));
    }

    public function create()
    {
        $editRow = '';
        return view('dboard.settings.page.page_inputs',compact('editRow'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'type' => 'required',
            'serial_no' => 'required'
        ]);

        $page = new Page;
        $page->title = ucfirst($request->title);
        $page->type = $request->type;
        $page->serial_no = $request->serial_no;
        $page->meta_description = $request->meta_description;
        $page->meta_keywords = $request->meta_keywords;
        $page->status = $request->status ? true : false ;
        $page->slug = str_slug($request->title, '-');

        if ($request->hasFile('image')) {
            $storeImage = new ImageUploadHelper;
            $width = $height = null;
            if($request->shape){
                $getShape = $storeImage->shapeMaker($request->shape, $request->width, $request->height);
                $width = $getShape['width'];
                $height = $getShape['height'];
            }
            // 1 mb = 1048576 bytes in binary which is countable for the image size here
            $image_name = $storeImage->uploadImage(null, $request->file('image'), 'img_content', $page->image, 5248576, $width, $height, $page->title, ['width' => null, 'height' => null, 'thumbStorageName' => null] );
            if($image_name == 'MaxSizeErr') {
                return back()->with('message_warning', 'Too large file (max limit:1mb)');
            }
            $page->image = $image_name;
        }

        $page->created_by = Auth::user()->id;
        $page->save();

        return redirect()->route('dboard.page.index')->with('message_success', 'Page has been created successfully.');
    }

    public function show($id)
    {
        $page = Page::select('id','title','slug','type','image','serial_no','meta_keywords','meta_description')->with(['sections.image_contents','sections.text_contents','sections.file_contents','sections.video_contents'])->find($id);

        if($page->sections && count($page->sections) > 0) {
            foreach ($page->sections as $s => $section) {
                if($section->type == 'productContent') {
                    $itemHelper = new ItemHelper;
                    if(count($section->product_contents) > 0) {
                        foreach ($section->product_contents as $pc => $productContent) {
                            $productContent->products = $itemHelper->displayProducts($productContent);
                        }
                    }
                }
            }
        }
        $editRow = null;
        return view('dboard.settings.page.page_details',compact('page','editRow'));
    }

    public function edit($id)
    {
        $editRow = Page::find($id);
        return view('dboard.settings.page.page_inputs',compact('editRow'));
    }

    public function update(Request $request, $id)
    {
        $page = Page::find($id);
        if($request->title){
            $page->title = $request->title;
        }
        if($request->subtitle){
            $page->subtitle = $request->subtitle;
        }
        if($request->pageType){
            $page->type = $request->pageType;
        }
        if($request->type){
            $page->type = $request->type;
        }

        $page->serial_no = $request->serial_no;
        $page->meta_description = $request->meta_description;
        $page->meta_keywords = $request->meta_keywords;
        $page->status = $request->status ? true : false ;

        if ($request->hasFile('image')) {
            $storeImage = new ImageUploadHelper;
            $width = $height = null;
            if($request->shape){
                $getShape = $storeImage->shapeMaker($request->shape, $request->width, $request->height);
                $width = $getShape['width'];
                $height = $getShape['height'];
            }
            // 1 mb = 1048576 bytes in binary which is countable for the image size here
            $image_name = $storeImage->uploadImage(null, $request->file('image'), 'img_content', $page->image, 5248576, $width, $height, $page->title, ['width' => null, 'height' => null, 'thumbStorageName' => null] );
            if($image_name == 'MaxSizeErr') {
                return back()->with('message_warning', 'Too large file (max limit:1mb)');
            }
            $page->image = $image_name;
        }

        $page->updated_by = Auth::user()->id;
        $page->update();

        return redirect()->route('dboard.page.show',$page->id)->with('message_success','Page has been updated successfully');
    }

    public function destroy($id)
    {
        $page = Page::with('sections')->find($id);

        if($page->sections && count($page->sections) > 0)
        {
            return back()->with('message_warning','sorry, this page has contents. Delete contents first.');
        }

        $page->delete();

        return back()->with('message_success','Page has been deleted successfully');

    }
}
