<?php

namespace App\Http\Controllers\Dboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;

class DboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('dboard');
    }

    public function dboard()
    {
    	return view('dboard.dboard');
    }
}
