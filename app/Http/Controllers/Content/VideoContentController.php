<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\Settings\Section;
use App\Models\Content\VideoContent;
use App\Models\Content\TextContent;

use Image;
use Storage;

// helpers
use Illuminate\Support\Str;
use App\Helpers\ImageUploadHelper;

class VideoContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('dboard');
    }

    public function index()
    {
        
    }

    public function create($id)
    {
        $editRow = '';
        $section = Section::find($id);
        return view('dboard.content.video_content.video_content_inputs',compact('section','editRow'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            // 'title' => 'required',
            // 'type' => 'required',
        ]);

        if($request->type == 'vdo_link' && !$request->vdo_link)
        {
            return back()->with('message_warning','Please, add a youtube video link or share link');
        }

        if($request->type == 'embed_code' && !$request->embed_code)
        {
            return back()->with('message_warning','Please, add a youtube embed code');
        }

        $pageId = Section::find($request->sectionId)->page_id;

        $videoContent = new VideoContent;
        $videoContent->section_id = $request->sectionId;
        $videoContent->text_content_id = $request->text_content_id;
        //$videoContent->type = $request->type;
        $videoContent->title = str_slug($request->title, '-');
        $videoContent->serial_no = $request->serial_no;
        $videoContent->description = $request->description;
        $videoContent->vdo_link = $request->vdo_link;
        $videoContent->embed_code = $request->embed_code;
        $videoContent->status = $request->status ? true : false;
        $videoContent->created_by = Auth::user()->id;
        $videoContent->save();

        return redirect()->route('dboard.page.show',$pageId)->with('message_success','Video Content has been created successfully.');
    }

    public function show($id)
    {
        return $id;
    }

    public function edit($id)
    {
        $editRow = VideoContent::find($id);
        $section = Section::find($editRow->section_id);
        return view('dboard.content.video_content.video_content_inputs',compact('section','editRow'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            // 'title' => 'required',
            // 'type' => 'required',
        ]);

        $pageId = Section::find($request->sectionId)->page_id;

        $videoContent = VideoContent::find($id);
        $videoContent->title = str_slug($request->title, '-');
        $videoContent->serial_no = $request->serial_no;
        $videoContent->description = $request->description;
        $videoContent->vdo_link = $request->vdo_link;
        $videoContent->embed_code = $request->embed_code;
        $videoContent->status = $request->status ? true : false;
        $videoContent->updated_by = Auth::user()->id;
        $videoContent->update();

        return redirect()->route('dboard.page.show',$pageId)->with('message_success','Video Content has been updated successfully.');
    }

    public function destroy($id)
    {
        $videoContent = VideoContent::find($id);
        $videoContent->delete();

        return back()->with('message_success','Video Content has been deleted successfully.');
    }

    public function status($id)
    {
        $videoContent = VideoContent::find($id);
        $videoContent->status = $videoContent->status ? false : true ;
        $videoContent->update();
        $status = $videoContent->status ? 'Activated' : 'Disabled' ;

        return back()->with('message_success','Video Content has been '.$status.' successfully.');
    }
}
