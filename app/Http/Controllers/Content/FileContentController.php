<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\Settings\Section;
use App\Models\Content\FileContent;
use App\Models\Content\TextContent;

use Image;
use Storage;

// helpers
use Illuminate\Support\Str;
use App\Helpers\ImageUploadHelper;

class FileContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('dboard');
    }

    public function index()
    {
        
    }

    public function create($id)
    {
        $editRow = '';
        $section = Section::find($id);
        return view('dboard.content.file_content.file_content_inputs',compact('section','editRow'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            // 'title' => 'required',
            // 'type' => 'required',
            'file' => 'required|mimes:pdf|max:512000',
        ]);

        $pageId = Section::find($request->sectionId)->page_id;

        $fileContent = new FileContent;
        $fileContent->section_id = $request->sectionId;
        $fileContent->type = 'pdf';
        $fileContent->serial_no = $request->serial_no;
        $fileContent->title = $request->title;
        $fileContent->description = $request->description;
        
        if ($request->hasFile('file')) {

            $storeFile = new FileUploadHelper;
            // 1 mb = 1048576 bytes in binary which is countable for the file size here
            $file_name = $storeFile->uploadFile($request->file('file'), 'file_content', null, 51200000, $fileContent->title);

            if($file_name == 'MaxSizeErr') {
                return back()->with('message_warning', 'Too large file (max limit:50mb)');
            }

            $fileContent->file = $file_name;
        }

        $fileContent->status = $request->status ? true : false ;
        $fileContent->created_by = Auth::user()->id;
        $fileContent->save();

        return redirect()->route('dboard.page.show',$pageId)->with('message_success','File Content has been created successfully.');
    }

    public function show($id)
    {
        return $id;
    }

    public function edit($id)
    {
        $editRow = FileContent::find($id);
        $section = Section::find($editRow->section_id);
        return view('dboard.content.file_content.file_content_inputs',compact('section','editRow'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            // 'title' => 'required',
            // 'type' => 'required',
            'file' => 'mimes:pdf|max:512000',
        ]);

        $pageId = Section::find($request->sectionId)->page_id;

        $fileContent = FileContent::find($id);
        $fileContent->serial_no = $request->serial_no;
        $fileContent->title = $request->title;
        $fileContent->description = $request->description;
        
        if ($request->hasFile('file')) {

            $storeFile = new FileUploadHelper;
            // 1 mb = 1048576 bytes in binary which is countable for the file size here
            $file_name = $storeFile->uploadFile($request->file('file'), 'file_content', $fileContent->file, 51200000, $fileContent->title);

            if($file_name == 'MaxSizeErr') {
                return back()->with('message_warning', 'Too large file (max limit:50mb)');
            }

            $fileContent->file = $file_name;
        }

        $fileContent->status = $request->status ? true : false ;
        $fileContent->updated_by = Auth::user()->id;
        $fileContent->update();

        return redirect()->route('dboard.page.show',$pageId)->with('message_success','File Content has been updated successfully.');
    }

    public function destroy($id)
    {
        $fileContent = FileContent::find($id);
        // check existing file
        if($fileContent->file != null) {
            if (Storage::disk('file_content')->has($fileContent->file)) {
                Storage::disk('file_content')->delete($fileContent->file);
            }
        }

        $fileContent->delete();

        return back()->with('message_success','File Content has been deleted successfully.');
    }

    public function status($id)
    {
        $fileContent = FileContent::find($id);
        $fileContent->status = $fileContent->status ? false : true ;
        $fileContent->update();
        $status = $fileContent->status ? 'Activated' : 'Disabled' ;

        return back()->with('message_success','File Content has been '.$status.' successfully.');
    }
}
