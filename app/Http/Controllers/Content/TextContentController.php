<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\Settings\Section;
use App\Models\Content\TextContent;
use App\Models\Content\ImageContent;
use App\Models\Content\FileContent;
use App\Models\Content\VideoContent;

use Image;
use Storage;

// helpers
use Illuminate\Support\Str;
use App\Helpers\ImageUploadHelper;

class TextContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('dboard');
    }

    public function index()
    {
        //
    }

    public function create($id)
    {
        $editRow = '';
        $section = Section::find($id);
        return view('dboard.content.text_content.text_content_inputs',compact('section','editRow'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            //'type' => 'required',
            //'description' => 'required',
        ]);

        $section = Section::find($request->sectionId);

        $textContent = new TextContent;
        $textContent->section_id = $request->sectionId;
        $textContent->type = $section->type;
        $textContent->title = $request->title;
        $textContent->description = $request->description;
        $textContent->serial_no = $request->serial_no;
        $textContent->status = $request->status ? true : false;
        
        if ($request->hasFile('image')) {
            $storeImage = new ImageUploadHelper;
            $width = $height = null;
            if($request->shape){
                $getShape = $storeImage->shapeMaker($request->shape, $request->width, $request->height);
                $width = $getShape['width'];
                $height = $getShape['height'];
            }
            // 1 mb = 1048576 bytes in binary which is countable for the image size here
            $image_name = $storeImage->uploadImage(null, $request->file('image'), 'img_content', null, 5248576, $width, $height, $textContent->title, ['width' => null, 'height' => null, 'thumbStorageName' => null] );
            if($image_name == 'MaxSizeErr') {
                return back()->with('message_warning', 'Too large file (max limit:1mb)');
            }
            $textContent->image = $image_name;
        }

        $textContent->created_by = Auth::user()->id;
        $textContent->save();

        return redirect()->route('dboard.page.show',$section->page_id)->with('message_success','Text Content has been created successfully.');
    }

    public function show($id)
    {
        $page = Page::with(['menu','sections'])->find($id);
        return view('dboard.settings.page.page_details',compact('page'));
    }

    public function edit($id)
    {
        $editRow = TextContent::find($id);
        $section = Section::find($editRow->section_id);
        return view('dboard.content.text_content.text_content_inputs',compact('section','editRow'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            //'type' => 'required',
            'description' => 'required',
            ]);

        $pageId = Section::find($request->sectionId)->page_id;

        $textContent = TextContent::find($id);
        //$textContent->type = $request->type;
        $textContent->title = $request->title;
        $textContent->description = $request->description;
        $textContent->serial_no = $request->serial_no;
        $textContent->status = $request->status ? true : false;
        
        if ($request->hasFile('image')) {
            $storeImage = new ImageUploadHelper;
            $width = $height = null;
            if($request->shape){
                $getShape = $storeImage->shapeMaker($request->shape, $request->width, $request->height);
                $width = $getShape['width'];
                $height = $getShape['height'];
            }
            // 1 mb = 1048576 bytes in binary which is countable for the image size here
            $image_name = $storeImage->uploadImage(null, $request->file('image'), 'img_content', $textContent->image, 5248576, $width, $height, $textContent->title, ['width' => null, 'height' => null, 'thumbStorageName' => null] );
            if($image_name == 'MaxSizeErr') {
                return back()->with('message_warning', 'Too large file (max limit:1mb)');
            }
            $textContent->image = $image_name;
        }

        $textContent->updated_by = Auth::user()->id;
        $textContent->update();

        return redirect()->route('dboard.page.show',$pageId)->with('message_success','Text Content has been updated successfully.');
    }

    public function destroy($id)
    {
        $textContent = TextContent::with(['image_contents','video_contents','file_contents'])->find($id);

        if($textContent->image)
        {
            if (Storage::disk('img_content')->has($textContent->image)) {
                Storage::disk('img_content')->delete($textContent->image);
            }

            $textContent->image = null;
        }

        // image contents
        if($textContent->image_contents->count() > 0)
        {
            foreach ($textContent->image_contents as $ti => $get_text_image) {
                $image = ImageContent::find($get_text_image->id);
                // check existing image
                if($image != null) {
                    if (Storage::disk('img_content')->has($image->image)) {
                        Storage::disk('img_content')->delete($image->image);
                    }
                }
                $image->delete();
            }
        }
        // file contents
        if($textContent->file_contents->count() > 0)
        {
            foreach ($textContent->file_contents as $ti => $get_text_file) {
                $file = FileContent::find($get_text_file->id);
                // check existing image
                if($file != null) {
                    if (Storage::disk('file_content')->has($file->file)) {
                        Storage::disk('file_content')->delete($file->file);
                    }
                }
                $file->delete();
            }
        }
        // video contents
        if($textContent->video_contents->count() > 0)
        {
            foreach ($textContent->video_contents as $ti => $get_text_video) {
                $video = VideoContent::find($get_text_video->id);
                $video->delete();
            }
        }
        $textContent->delete();

        return back()->with('message_success','Text Content has been deleted successfully.');
    }

    public function status($id)
    {
        $textContent = TextContent::find($id);
        $textContent->status = $textContent->status ? false : true ;
        $textContent->update();
        $status = $textContent->status ? 'Activated' : 'Disabled' ;

        return back()->with('message_success','Text Content has been '.$status.' successfully.');
    }
}
