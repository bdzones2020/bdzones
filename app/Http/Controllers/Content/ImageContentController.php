<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\Settings\Section;
use App\Models\Content\ImageContent;
use App\Models\Content\TextContent;

use Image;
use Storage;

// helpers
use Illuminate\Support\Str;
use App\Helpers\ImageUploadHelper;

class ImageContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('dboard');
    }

    public function index()
    {
        
    }

    public function create($sectionId)
    {
        $editRow = '';
        $section = Section::find($sectionId);
        return view('dboard.content.image_content.multiple_image_content_inputs',compact('editRow','section'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            //'type' => 'required',
            //'image' => 'required|mimes:jpg,jpeg,png,gif',
        ]);

        $section = Section::find($request->sectionId);
        $shape = null;

        if ($request->uploadedImages && count($request->uploadedImages) > 0) {
            $storeImage = new ImageUploadHelper;

            $width = $height = null;
            if($request->shape){
                $getShape = $storeImage->shapeMaker($request->shape, $request->width, $request->height);
                $width = $getShape['width'];
                $height = $getShape['height'];
                $shape = $width.' * '.$height;
            }

            foreach ($request->uploadedImages as $k => $uploadedImage) {
                
                $imageContent = new ImageContent;
                $imageContent->section_id = $request->sectionId;
                $imageContent->type = $section->type;
                $imageContent->title = $request->title && $request->title[$k] ? $request->title[$k] : NULL;
                $imageContent->link = $request->link && $request->link[$k] ? $request->link[$k] : NULL;
                $imageContent->caption = $request->caption && $request->caption[$k] ? $request->caption[$k] : NULL;
                $imageContent->serial_no = $request->serialNo && $request->serialNo[$k] ? $request->serialNo[$k] : 0;
                $imageContent->status = true;
                $imageContent->shape = $shape;
                $imageContent->created_by = Auth::user()->id;
                $imageContent->save();

                $encodeType = explode('/', explode(';', $uploadedImage)[0])[1];

                // 1 mb = 1048576 bytes in binary which is countable for the image size here
                $image_name = $storeImage->uploadImage($encodeType, $uploadedImage, 'img_content', null, 5248576, $width, $height, $imageContent->id, ['width' => null, 'height' => null, 'thumbStorageName' => null] );
                // get max size alert
                if($image_name == 'MaxSizeErr') {
                    return back()->with('message_warning', 'Too large file (max limit:1mb)');
                }

                $imageContent->image = $image_name;
                $imageContent->update();
            }
        }

        return redirect()->route('dboard.page.show',$section->page_id)->with('message_success','Slide Image has been created successfully.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $editRow = ImageContent::find($id);
        $section = Section::find($editRow->section_id);
        return view('dboard.content.image_content.image_content_inputs',compact('editRow','section'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            //'type' => 'required',
            'image' => 'mimes:jpg,jpeg,png,gif',
        ]);

        $imageContent = ImageContent::find($id);
        $imageContent->title = $request->title ? $request->title : NULL;
        $imageContent->link = $request->link ? $request->link : NULL;
        $imageContent->caption = $request->caption ? $request->caption : NULL;
        $imageContent->serial_no = $request->serial_no;
        $imageContent->status = $request->status ? true : false;
        
        if ($request->hasFile('image')) {
            $storeImage = new ImageUploadHelper;
            $width = $height = null;
            if($request->shape){
                $getShape = $storeImage->shapeMaker($request->shape, $request->width, $request->height);
                $width = $getShape['width'];
                $height = $getShape['height'];
                $imageContent->shape = $width.' * '.$height;
            }
            // 1 mb = 1048576 bytes in binary which is countable for the image size here
            $image_name = $storeImage->uploadImage(null,$request->file('image'), 'img_content', $imageContent->image, 5248576, $width, $height, $imageContent->title, ['width' => null, 'height' => null, 'thumbStorageName' => null] );
            // get max size alert
            if($image_name == 'MaxSizeErr') {
                return back()->with('message_warning', 'Too large file (max limit:1mb)');
            }

            $imageContent->image = $image_name;
        }

        $imageContent->updated_by = Auth::user()->id;
        $imageContent->update();

        $pageId = Section::find($imageContent->section_id)->page_id;
        return redirect()->route('dboard.page.show',$pageId)->with('message_success','Slide Image has been updated successfully.');
    }

    public function destroy($id)
    {
        $imageContent = ImageContent::find($id);

        if (Storage::disk('img_content')->has($imageContent->image)) {
            Storage::disk('img_content')->delete($imageContent->image);
        }

        $imageContent->delete();

        return back()->with('message_success','Image Content has been deleted successfully.');
    }

    public function status($id)
    {
        $imageContent = ImageContent::find($id);
        $imageContent->status = $imageContent->status ? false : true ;
        $imageContent->update();
        $status = $imageContent->status ? 'Activated' : 'Disabled' ;

        return back()->with('message_success','Image Content has been '.$status.' successfully.');
    }
}
