<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\Settings\Page;

class WebController extends Controller
{
    public function index()
    {
    	$page = Page::select('id','title','slug','status','meta_description','meta_keywords')->with(['active_sections.active_image_contents','active_sections.active_text_contents','active_sections.active_article'])->where('status',true)->where('slug','home')->first();
    	//return $page;
    	return view('web.home', compact('page'));
    }
}
