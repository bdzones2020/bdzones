<?php

use Illuminate\Support\Facades\URL;

function getUrl()
{
	$urls = explode('/', url()->current());
    $counter = count($urls) -1 ;
    return $urls[$counter];
	// return $urls = count(explode('/', url()->current()));
	// return $urls = url()->current();
	// return $urls = url()->previous();
}

function getFullUrl()
{
	return url()->full();
}

function slugToTitle($slug)
{
	$title = '';
	foreach (explode(' ', str_replace('-', ' ', $slug)) as $k => $word) {
		$title .= $k != 0 ? ' ' . ucfirst($word) : ucfirst($word); 
	}
	return $title;
}

function checkUrl($item = null)
{
	if($item)
	{
		return in_array($item, explode('/', url()->current())) ? true : false ;
	}else{
		return false;
	}
}

function getGroupContentType($properties)
{
	if(strpos($properties, ','))
	{
		return ucfirst(str_replace('Content', '', explode(',', $properties)[0]));
	}
}