<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Auth;

class DboardMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->weight >= 59.99 && Auth::user()->status == 1)
        {
            return $next($request);
        }else{
        	abort(403);
        	//return redirect('home')->with('message_warning','Sorry, you are not permitted to enter here. Please contact to your system admin.');
        }
    }
}
